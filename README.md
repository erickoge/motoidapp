# MotoidAPP

Motostation APP Using React Native

- npm install -g create-react-native-app
- create-react-native-app MotoidApp
- Install NodeJS Python Jdk8
- npm install -g react-native-cli
- npm start
- Install expo client in your android devise and scan the above obtained QR code.
- npm run eject : to using regular react native project
- install android studio
- configure the AVD Manager click on the respective icon in the menu bar
- Choose a device definition, Nexus 5X is suggestable
- System Image window. Select the x86 Images tab.
- select Marshmallow and click on next.
- After configuring your virtual device click on the play button under the Actions column to start your android emulator.
- execute the react-native run-android command
- click on the android emulator press ctrl+m then, select Enable Hot Reloading option.
